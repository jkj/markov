# Markov Process AI Text Generator

You don't need DNNs to generate stuff, nor hours of training!

## Compile

* Microsoft:  
`cl /EHsc /Ox mark.cpp`

* Linux:  
`g++ -std=c++11 -O2 -o mark mark.cpp`

## Run

* Shakespeare:  
`./mark shakespeare_input.txt -o 10 -gen 5000`

* IF titles:  
`./mark ifdb-titles-2018.txt -o 6 -gen 2000 | sort | uniq`

## Variation

Mess with the order, `-o`, option. This is the number of character to use in the Markov history. About 5-7 is good for approx one word length. Small numbers will produce less formed words, but more variation.




