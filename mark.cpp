/*
 *
 *
 *   ______              _
 *   | ___ \            | |
 *   | |_/ / _ __  __ _ | |__   _ __ ___    __ _  _ __
 *   | ___ \| '__|/ _` || '_ \ | '_ ` _ \  / _` || '_ \
 *   | |_/ /| |  | (_| || | | || | | | | || (_| || | | |
 *   \____/ |_|   \__,_||_| |_||_| |_| |_| \__,_||_| |_|
 *
 *
 *  "The creative principle which lies realized in the whole world"
 *
 *  Copyright (c) Strand Games 2017.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to
 *  deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 *  sell copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 *  THE SOFTWARE IS PROVIDED "AS IS," WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 * 
 *  contact@strandgames.com
 */
 

#include <time.h>
#include <string.h>
#include <assert.h>
#include <iostream>
#include <fstream>

#include <string>
#include <map>

#include "rand.h"

struct Markov
{
    static const int maxOrder = 16;
    
    char        _buf[maxOrder+1];
    int         _order;

    struct Dictionary
    {
        typedef std::string string;
        typedef std::map<char, float>  Counter;
        typedef std::map<string, Counter> MapT;
        
        MapT            _map;
        Ranq1           _ran;

        Dictionary()
        {
            _ran.seed(clock());
        }

        void count(const char* s, char c)
        {
            Counter& co = _map[s];
            co[c] += 1;
        }

        void normalise()
        {
            for (MapT::iterator it = _map.begin(); it != _map.end(); ++it)
            {
                Counter& co = it->second;
                
                float total = 0;
                for (Counter::iterator ij = co.begin();
                     ij != co.end(); ++ij) total += ij->second;

                assert(total != 0);

                for (Counter::iterator ij = co.begin();
                     ij != co.end(); ++ij) ij->second /= total;
            }
        }

        char gen(const string& key) const
        {
            char c = '~';
            MapT::const_iterator it = _map.find(key);
            if (it != _map.cend())            
            {
                float x = _ran.genFloat();
                const Counter& co = it->second;
                for (Counter::const_iterator ij = co.cbegin();
                     ij != co.cend(); ++ij)                
                {
                    x -= ij->second;
                    if (x <= 0)
                    {
                        c = ij->first;
                        break;
                    }
                }
            }
            return c;
        }

        void printCases(std::ostream& os, const string& key) const
        {
            os << "cases for " << key << std::endl;
            MapT::const_iterator it = _map.find(key);
            if (it != _map.cend())
            {
                const Counter& co = it->second;
                for (Counter::const_iterator ij = co.cbegin();
                     ij != co.cend(); ++ij)
                {
                    os << ij->first << ' ' << ij->second << std::endl;
                }
            }
        }
        
    };

    Dictionary          _history;

    void train(std::istream& in, int order)
    {
        assert(order <= maxOrder);
        _order = order;
        
        _pad();
        
        int c;
        while ((c = in.get()) != EOF)
        {
            _history.count(_buf, c);
            _enbuf(c);
        }

        _history.normalise();
    }

    void generateText(std::ostream& os, int n)
    {
        _pad();
        for (int i = 0; i < n; ++i)
        {
            char c = _history.gen(_buf);
            _enbuf(c);
            os << c;
        }
    }

    void _pad()
    {
        // fill with pad
        int i;
        for (i = 0; i < _order; ++i) _buf[i] = '~';
        _buf[i] = 0;
    }

    void _enbuf(char c)
    {
        int i;
        for (i = 0; i < _order-1; ++i) _buf[i] = _buf[i+1];
        _buf[i] = (char)c;
    }

};


using namespace std;

int main(int argc, char** argv)
{
    const char* infile = 0;
    int order = 4; // default
    int genAmount = 1000;

    for (int i = 1; i < argc; ++i)
    {
        if (argv[i][0] == '-')
        {
            if (!strcmp(argv[i], "-o") && i < argc-1)
            {
                order = atoi(argv[++i]);
                cerr << "order " << order << endl;
            }
            else if (!strcmp(argv[i], "-gen") && i < argc-1)
            {
                genAmount = atoi(argv[++i]);
                cerr << "generate amount " << genAmount << endl;
            }
        }
        else
        {
            infile = argv[i];
        }
    }

    if (!infile)
    {
        cerr << "Usage " << argv[0] << " [-o <order>] [-gen <amount>] <infile>\n";
        return -1;
    }

    ifstream in(infile);
    if (in.good())
    {
        Markov mk;
        mk.train(in, order);

        mk.generateText(cout, genAmount);
        cout << endl;
    }
    else cerr << "Can't open '" << infile << "'\n";

    return 0;
}
